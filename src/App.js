import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Budget from './components/Budget';
import AvailFunds from './components/AvailFunds';
import TotalExpenses from './components/TotalExpenses';
import ExpenseList from './components/ExpenseList';
import ExpenseForm from './components/ExpenseForm';
import { AppProvider } from './context/AppContext';

const App = () => {
  return (
    <AppProvider>
      <div className='container'>
        <h1 className='mt-4'>For The Love of Money</h1>
        <div className='row mt-3'>
          <div className='col-sm'>
            <Budget />
          </div>
          <div className='col-sm'>
            <AvailFunds />
          </div>
          <div className='col-sm'>
            <TotalExpenses />
          </div>
        </div>

        <h3 className='mt-3'>Expenses</h3>
        <div className='row mt-3'>
          <div className='col-sm'>
            <ExpenseList />
          </div>
        </div>

        <h3 className='mt-3'>Add Expense</h3>
        <div className='row mt-3'>
          <div className='col-sm'>
            <ExpenseForm />
          </div>
        </div>
      </div>
    </AppProvider>
  );
}

export default App;