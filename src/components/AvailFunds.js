import React, { useContext } from 'react';
import { AppContext } from '../context/AppContext';

const AvailFunds = () => {
    const { expenses, budget } = useContext(AppContext);
    
    const allExpenses = expenses.reduce((total, item) => {
        return (total += item.cost);
    }, 0);

    const alertType = allExpenses > budget ? 'alert-danger': 'alert-success';

    return (
        <div className={`alert ${alertType}`}>
            <span>Available Funds: ${budget - allExpenses}</span>
        </div>
    );
}

export default AvailFunds;