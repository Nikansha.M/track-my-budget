import { createContext, useReducer } from "react";

const AppReducer = (state, action) => {
    switch(action.type){
        case 'ADD_EXPENSE':
            return {
                ...state, 
                expenses: [...state.expenses, action.payload],
            }
        case 'DELETE_EXPENSE':
            return {
                ...state,
                expenses: state.expenses.filter((expense) => expense.id !== action.payload),
            }

        case 'SET_BUDGET':
            return {
                ...state,
                budget: action.payload,
            }
            
        default:
            return state;
    }
};

const initialState = {
    budget: 2500,
    expenses: [
        { id: 15, name: 'holiday', cost: 65 },
        { id: 16, name: 'gas', cost: 80 },
        { id: 17, name: 'car maintenance', cost: 235 },
    ],
};

export const AppContext = createContext();

export const AppProvider = (props) => {
    const [state, dispatch] = useReducer(AppReducer, initialState);

    return(
        <AppContext.Provider 
            value={{
                budget: state.budget,
                expenses: state.expenses, 
                dispatch,
            }}
        >
            {props.children}
        </AppContext.Provider>
    );
};